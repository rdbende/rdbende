### Hello, I'm rdbende :wave:

- :snake: I like making beautiful user interfaces with Python and Tkinter
- :heart_eyes_cat: You might wanna check out my ttk themes. They can even make Tkinter look like **Windows 11**: [Sun-Valley-ttk-theme](https://github.com/rdbende/Sun-Valley-ttk-theme)
- :crab: Lately I've been into GUI development using Rust and GTK 4
- :musical_keyboard: I'm also interested in sound engineering, synths and electronic music, you can find some related projects on my profile
- :penguin: Permanent Linux user :muscle: (Fedora with Vanilla GNOME)
- :mailbox: Feel free to [contact me via email](mailto:rdbende@proton.me), or [chat with me on Matrix](https://matrix.to/#/@rdbende:matrix.org)

</br>
<p align="center">
  <img src="https://github-readme-stats.vercel.app/api?username=rdbende&show_icons=true&count_private=true&bg_color=30,e96443,904e95&icon_color=fafafa&text_color=fafafa&title_color=fafafa&border_color=fafafa&border_radius=20&include_all_commits=true&line_height=30">
</p>
